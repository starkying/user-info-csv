import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetUserInfo {

    private final String COLUMN_0 = "Name";
    private final String COLUMN_1 = "Email";
    private final String COLUMN_2 = "Phone";
    private final String FILE_NAME = "users.csv";

    public void get(List<String> infos) {
        try {
            File csvFile = new File(FILE_NAME);
            if (!csvFile.exists()) {
                FileWriter csvWriter = new FileWriter(FILE_NAME);
                csvWriter.append(COLUMN_0);
                csvWriter.append(",");
                csvWriter.append(COLUMN_1);
                csvWriter.append(",");
                csvWriter.append(COLUMN_2);
                csvWriter.append("\n");
                csvWriter.append(String.join(",", infos));
                csvWriter.append("\n");
                csvWriter.flush();
                csvWriter.close();
            } else {
                FileWriter csvWriter = new FileWriter(FILE_NAME,true);
                csvWriter.append(String.join(",", infos));
                csvWriter.append("\n");
                csvWriter.flush();
                csvWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getDomain() throws IOException {
        System.out.println("getDomain() 호출");
        File pathToCsv = new File("./users.csv");
        BufferedReader csvReader = new BufferedReader(new FileReader(pathToCsv));

        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            String email = data[1];
            if (email.equals(COLUMN_1))   continue;
            String domain = email.substring(email.indexOf('@') + 1);
            System.out.println("domain ==> " + domain);
        }
        csvReader.close();
    }
    
    public void lastnameStats() throws IOException {
        System.out.println("lastnameStats() 호출");
        File pathToCsv = new File("./users.csv");
        BufferedReader csvReader = new BufferedReader(new FileReader(pathToCsv));

        Map<String, Integer> lastNames = new HashMap<>();
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            String name = data[0];
            if (name.equals(COLUMN_0))   continue;
            String lastName = name.split(" ")[1];
            lastNames.put(lastName, lastNames.getOrDefault(lastName, 0) + 1);
        }
        csvReader.close();
        int size = 0;
        for (String key : lastNames.keySet()) {
            if (lastNames.get(key) > 1) {
                size++;
            }
        }
        System.out.println(size + "개의 Lastname이 겹칩니다.");
    }
}

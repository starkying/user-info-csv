# User Info CSV

> 사용자의 정보를 입력 받아 CSV 파일에 저장합니다.

## Example
```shell
javac Main.java
java Main "Gyumin Kim" "starkying@gmail.com" "010.7192.1292"
java Main "Illbun Kim" "ill@gmail.com" "010.2343.3463"
java Main "Jiyoung Lee" "ergsl334@naver.com" "010.3464.3275"
java Main "Chanho Park" "qkrcksgh@hanmail.net" "010.3462.5674"
java Main "Jongsu Lee" "jslee23@gmail.com" "010.9902.2354"
...
```